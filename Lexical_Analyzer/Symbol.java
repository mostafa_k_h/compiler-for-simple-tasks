package Lexical_Analyzer;

public class Symbol {
    public final static int NUM = 256, ID1 = 257,ID2 = 258, TRUE = 259, FALSE = 260
            , SIN = 261, COS = 262, TAN = 263, COT = 264, log = 265, exp = 266, e = 267
            ,PI = 268, DIV = 269, MOD = 270 , SQRT = 271 , SINH = 272
            , COSH = 273 , TANH = 274, COTH = 275 , ARCsin = 276, ARCcos = 277, Arctan = 278
            , Arccot = 279;

}
