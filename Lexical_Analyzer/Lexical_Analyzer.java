package Lexical_Analyzer;

import java.util.*;

public class Lexical_Analyzer {

    char[] characterslist;
    public static int line = 1;
    public static Hashtable<String, Word> symboltable = new Hashtable<String, Word>();

    void addtable(Word t) {
        symboltable.put(t.lexeme, t);
    }

    public Lexical_Analyzer() {
        addtable(new Word(Symbol.TRUE, "true"));
        addtable(new Word(Symbol.FALSE, "false"));
        addtable(new Word(Symbol.e, "e"));
        addtable(new Word(Symbol.PI, "pi"));
        addtable(new Word(Symbol.DIV, "div"));
        addtable(new Word(Symbol.MOD, "mod"));
        addtable(new Word(Symbol.SQRT, "sqrt"));
        addtable(new Word(Symbol.log, "log"));
        addtable(new Word(Symbol.exp, "exp"));
        addtable(new Word(Symbol.SIN, "sin"));
        addtable(new Word(Symbol.COS, "cos"));
        addtable(new Word(Symbol.TAN, "tan"));
        addtable(new Word(Symbol.COT, "cot"));
        addtable(new Word(Symbol.SINH, "sinh"));
        addtable(new Word(Symbol.COSH, "cosh"));
        addtable(new Word(Symbol.TANH, "tanh"));
        addtable(new Word(Symbol.ARCsin, "Arcsin"));
        addtable(new Word(Symbol.ARCcos, "Arccos"));
        addtable(new Word(Symbol.Arctan, "Arctan"));
        addtable(new Word(Symbol.Arccot, "Arccot"));

    }

    private static char lookhead;
    private static int numofchar = 0;

    public Token Checker() {
        for (; numofchar < characterslist.length; numofchar++) {
            lookhead = characterslist[numofchar];
            if (lookhead == ' ') {
            }
            if (lookhead == '\t') {
            } else if (lookhead == '\n') {
                line++;
                return new Token('\n');
            } else {
                break;
            }
        }

        if (Character.isDigit(lookhead)) {
            double num_integer = 0, num_decimal = 0.0;
            while (Character.isDigit(lookhead)) {
                num_integer = 10 * num_integer + Character.getNumericValue(lookhead);
                numofchar++;
                lookhead = characterslist[numofchar];
            }
            int numberofdecimal = 0;
            if (lookhead == ',' || lookhead == '.') {
                numofchar++;
                while (Character.isDigit(characterslist[numofchar])) {
                    lookhead = characterslist[numofchar];
                    numofchar++;
                    num_decimal = 10 * num_decimal + Character.getNumericValue(lookhead);
                    numberofdecimal++;
                }
            }
            num_integer += num_decimal / Math.pow(10, numberofdecimal);
            return new Numbers(num_integer);
        }

        if (Character.isLetter(lookhead)) {
            StringBuffer b = new StringBuffer();
            do {
                b.append(lookhead);
                lookhead = characterslist[++numofchar];
            } while (Character.isLetterOrDigit(lookhead));
            String var1 = b.toString();
            if (var1.equals("x")) {
                Word w = (Word) symboltable.get(var1);
                if (w != null) {
                    return w;
                }
                w = new Word(Symbol.ID1, var1);

                symboltable.put(var1, w);
                return w;
            } else if (var1.equals("y")) {
                Word w = (Word) symboltable.get(var1);
                if (w != null) {
                    return w;
                }
                w = new Word(Symbol.ID2, var1);

                symboltable.put(var1, w);
                return w;
            }
            Word w = (Word) symboltable.get(var1);
            if (w != null) {
                return w;
            }
            w = new Word(Symbol.ID1, var1);

            symboltable.put(var1, w);
            return w;

        }
        Token t = new Token(lookhead);
        numofchar++;
        return t;
    }

    public Token Checker(char[] chars) {
        characterslist = new char[chars.length];
        for (int j = 0; j < chars.length; j++) {
            characterslist[j] = chars[j];
        }

        for (; numofchar < chars.length; numofchar++) {
            lookhead = chars[numofchar];
            if (lookhead == ' ' || lookhead == '\t') {
                continue;
            } else if (lookhead == '\n') {
                line++;
            } else {
                break;
            }
        }

        if (Character.isDigit(lookhead)) {
            double num_integer = 0.0, num_decimal = 0.0;
            while (Character.isDigit(lookhead)) {
                num_integer = 10 * num_integer + Character.getNumericValue(lookhead);
                numofchar++;
                lookhead = characterslist[numofchar];
            }
            int numberofdecimal = 0;
            if (lookhead == ',' || lookhead == '.') {
                numofchar++;
                while (Character.isDigit(characterslist[numofchar])) {
                    lookhead = characterslist[numofchar];
                    numofchar++;
                    num_decimal = 10 * num_decimal + Character.getNumericValue(lookhead);
                    numberofdecimal++;
                }
            }
            num_integer += num_decimal / Math.pow(10, numberofdecimal);
            return new Numbers(num_integer);
        }

        if (Character.isLetter(lookhead)) {
            StringBuilder sb = new StringBuilder();
            do {
                sb.append(lookhead);
                lookhead = characterslist[++numofchar];
            } while (Character.isLetterOrDigit(lookhead));
            String var2 = sb.toString();
            if (var2.equals("x")) {
                Word w = symboltable.get(var2);
                if (w != null) {
                    return w;
                }
                w = new Word(Symbol.ID1, var2);
                symboltable.put(var2, w);
                return w;
            }
            if (var2.equals("y")) {
                Word w = symboltable.get(var2);
                if (w != null) {
                    return w;
                }
                w = new Word(Symbol.ID2, var2);
                symboltable.put(var2, w);
                return w;
            }
            Word w = symboltable.get(var2);
            if (w != null) {
                return w;
            }
            w = new Word(Symbol.ID1, var2);
            symboltable.put(var2, w);
            return w;
        }

        Token t = new Token(lookhead);
        numofchar++;
        return t;
    }
}
