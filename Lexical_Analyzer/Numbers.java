package Lexical_Analyzer;

public class Numbers extends Token {

    public final double value;

    public Numbers(double v) {
        super(Symbol.NUM);
        value = v;
    }

}
