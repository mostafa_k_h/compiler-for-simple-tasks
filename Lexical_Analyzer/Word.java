package Lexical_Analyzer;

public class Word extends Token {

    public final String lexeme;
    public static double value;

    public Word(int T, String val) {
        super(T);
        lexeme = new String(val);
    }

}
