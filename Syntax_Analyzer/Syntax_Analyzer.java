package Syntax_Analyzer;

import Lexical_Analyzer.*;

import java.util.*;

public class Syntax_Analyzer {

    Lexical_Analyzer lexer = new Lexical_Analyzer();
    ArrayList<Character> characters = new ArrayList<>();
    static ArrayList<String> list_of_postfix = new ArrayList<>();
    HashSet<Character> connector = new HashSet<>();
    public Stack stack_calculation = new Stack();
    char[] chars;
    double first_var = 0;
    double second_var = 0;
    static int identifier = 0;
    static double digit = 0;
    boolean valuable = false;
    boolean Negative = false;
    String negetives_before = "Negative operator";
    static Token lookahead;
    static Token forhead;

    public Syntax_Analyzer(String s) throws Exception {
        Scanner x = new Scanner(System.in);

        if (var(s)) {
            if (identifier == 1) {
                first_var = x.nextDouble();
            } else if (identifier == 2) {
                first_var = x.nextDouble();
                second_var = x.nextDouble();

            }
        }
        chars = new char[s.length() + 1];
        for (int i = 0; i < s.length(); i++) {
            chars[i] = s.charAt(i);
        }
        lookahead = lexer.Checker(chars);
    }

    public void Addition_Subtraction() throws Exception {
        Multiplication_division();
        while (true) {
            if (lookahead.symbol == '+') {
                match(new Token('+'));
                Multiplication_division();
                list_of_postfix.add("+ ");
                double b = ((Numbers) stack_calculation.pop()).value;
                double a = ((Numbers) stack_calculation.pop()).value;
                stack_calculation.push(new Numbers(a + b));
            } else if (lookahead.symbol == '-' && negetives_before.equals("CNL")) {
                match(new Token('-'));
                Multiplication_division();
                list_of_postfix.add("- ");
                double b = ((Numbers) stack_calculation.pop()).value;
                double a = ((Numbers) stack_calculation.pop()).value;
                stack_calculation.push(new Numbers(a - b));
            } else {
                return;
            }
        }
    }

    private void Multiplication_division() throws Exception {
        Power();
        while (true) {
            if (lookahead.symbol == '*') {
                match(new Token('*'));
                Power();
                list_of_postfix.add("* ");
                double b = ((Numbers) stack_calculation.pop()).value;
                double a = ((Numbers) stack_calculation.pop()).value;
                stack_calculation.push(new Numbers(a * b));
            } else if (lookahead.symbol == '/') {
                match(new Token('/'));
                Power();
                list_of_postfix.add("/ ");
                double b = ((Numbers) stack_calculation.pop()).value;
                double a = ((Numbers) stack_calculation.pop()).value;
                stack_calculation.push(new Numbers(a / b));
            } else if (lookahead.symbol == Symbol.DIV) {
                match(new Token(Symbol.DIV));
                Power();
                list_of_postfix.add("div ");
                double b = ((Numbers) stack_calculation.pop()).value;
                double a = ((Numbers) stack_calculation.pop()).value;
                stack_calculation.push(new Numbers((int) (a / b)));
            } else if (lookahead.symbol == Symbol.MOD) {
                match(new Token(Symbol.MOD));
                Power();
                list_of_postfix.add("mod ");
                double b = ((Numbers) stack_calculation.pop()).value;
                double a = ((Numbers) stack_calculation.pop()).value;
                stack_calculation.push(new Numbers(a % b));
            } else {
                return;
            }
        }
    }

    private void Power() throws Exception {
        Functions();
        while (true) {
            if (lookahead.symbol == '-' && negetives_before.equals("Negative operator")) {
                Negative = true;
                match((new Token('-')));
                Functions();
                double a = 0;
                double b = ((Numbers) stack_calculation.pop()).value;
                stack_calculation.push(new Numbers(a - b));
            }
            if (lookahead.symbol == '^') {
                match(new Token('^'));
                Functions();
                list_of_postfix.add("^ ");
                double b = ((Numbers) stack_calculation.pop()).value;
                double a = ((Numbers) stack_calculation.pop()).value;
                stack_calculation.push(new Numbers(Math.pow(a, b)));
            } else if (lookahead.symbol == Symbol.e) {
                match((new Token(Symbol.e)));
                stack_calculation.push(new Numbers(Math.E));
                list_of_postfix.add("e ");
            } else if (lookahead.symbol == Symbol.PI) {
                match((new Token(Symbol.PI)));
                stack_calculation.push(new Numbers(Math.PI));
                list_of_postfix.add("pi ");
            } else {
                return;
            }
        }
    }

    private void Functions() throws Exception {
        while (true) {

            if (lookahead.symbol == Symbol.SIN) {
                match(new Token(Symbol.SIN));
                match(new Token('('));
                Addition_Subtraction();
                match(new Token(')'));
                list_of_postfix.add("sin ");
                stack_calculation.push(new Numbers(Math.sin(((Numbers) stack_calculation.pop()).value)));
            } else if (lookahead.symbol == Symbol.SINH) {
                match(new Token(Symbol.SINH));
                match(new Token('('));
                Addition_Subtraction();
                match(new Token(')'));
                list_of_postfix.add("sinh ");
                stack_calculation.push(new Numbers(Math.sinh((((Numbers) stack_calculation.pop()).value))));
            } else if (lookahead.symbol == Symbol.ARCsin) {
                match(new Token(Symbol.ARCsin));
                match(new Token('('));
                Addition_Subtraction();
                match(new Token(')'));
                list_of_postfix.add("Arcsin ");
                stack_calculation.push(new Numbers(Math.asin(((Numbers) stack_calculation.pop()).value)));
            } else if (lookahead.symbol == Symbol.COS) {
                match(new Token(Symbol.COS));
                match(new Token('('));
                Addition_Subtraction();
                match(new Token(')'));
                list_of_postfix.add("cos ");
                stack_calculation.push(new Numbers(Math.cos((((Numbers) stack_calculation.pop()).value))));
            } else if (lookahead.symbol == Symbol.COSH) {
                match(new Token(Symbol.COSH));
                match(new Token('('));
                Addition_Subtraction();
                match(new Token(')'));
                list_of_postfix.add("cosh ");
                stack_calculation.push(new Numbers(Math.cosh((((Numbers) stack_calculation.pop()).value))));
            } else if (lookahead.symbol == Symbol.ARCcos) {
                match(new Token(Symbol.ARCcos));
                match(new Token('('));
                Addition_Subtraction();
                match(new Token(')'));
                list_of_postfix.add("Arccos ");
                stack_calculation.push(new Numbers(Math.acos(((Numbers) stack_calculation.pop()).value)));
            } else if (lookahead.symbol == Symbol.TAN) {
                match(new Token(Symbol.TAN));
                match(new Token('('));
                Addition_Subtraction();
                match(new Token(')'));
                list_of_postfix.add("tan ");
                stack_calculation.push(new Numbers(Math.tan((((Numbers) stack_calculation.pop()).value))));
            } else if (lookahead.symbol == Symbol.TANH) {
                match(new Token(Symbol.TANH));
                match(new Token('('));
                Addition_Subtraction();
                match(new Token(')'));
                list_of_postfix.add("tanh ");
                stack_calculation.push(new Numbers(Math.tanh((((Numbers) stack_calculation.pop()).value))));
            } else if (lookahead.symbol == Symbol.Arctan) {
                match(new Token(Symbol.Arctan));
                match(new Token('('));
                Addition_Subtraction();
                match(new Token(')'));
                list_of_postfix.add("Arctan ");
                stack_calculation.push(new Numbers(Math.atan(((Numbers) stack_calculation.pop()).value)));
            } else if (lookahead.symbol == Symbol.COT) {
                match(new Token(Symbol.COT));
                match(new Token('('));
                Addition_Subtraction();
                match(new Token(')'));
                list_of_postfix.add("cot ");
                stack_calculation.push(new Numbers(1.0 / Math.tan((((Numbers) stack_calculation.pop()).value))));
            } else if (lookahead.symbol == Symbol.COTH) {
                match(new Token(Symbol.COTH));
                match(new Token('('));
                Addition_Subtraction();
                match(new Token(')'));
                list_of_postfix.add("coth");
                stack_calculation.push(new Numbers(1.0 / Math.tanh((((Numbers) stack_calculation.pop()).value))));
            } else if (lookahead.symbol == Symbol.Arccot) {
                match(new Token(Symbol.Arccot));
                match(new Token('('));
                Addition_Subtraction();
                match(new Token(')'));
                list_of_postfix.add("Arccot ");
                stack_calculation.push(new Numbers(1.0 / Math.atan((((Numbers) stack_calculation.pop()).value))));
            } else if (lookahead.symbol == Symbol.SQRT) {
                match(new Token(Symbol.SQRT));
                match(new Token('('));
                Addition_Subtraction();
                match(new Token(')'));
                list_of_postfix.add("sqrt ");
                stack_calculation.push(new Numbers(Math.sqrt(((Numbers) stack_calculation.pop()).value)));
            } else if (lookahead.symbol == Symbol.log) {
                match(new Token(Symbol.log));
                match(new Token('('));
                Addition_Subtraction();
                match(new Token(')'));
                list_of_postfix.add("log ");
                stack_calculation.push(new Numbers(Math.log(((Numbers) stack_calculation.pop()).value)));
            } else if (lookahead.symbol == Symbol.exp) {
                match(new Token(Symbol.exp));
                match(new Token('('));
                Addition_Subtraction();
                match(new Token(')'));
                list_of_postfix.add("exp ");
                stack_calculation.push(new Numbers(Math.exp(((Numbers) stack_calculation.pop()).value)));
            } else if (lookahead.symbol == Symbol.NUM) {
                DigitOrLetter();
                if (lookahead.symbol == 257) {
                    throw new Error(" Making a variable with a number is then impossible "
                            + "in Line " + lexer.line);
                } else {
                    list_of_postfix.add(String.valueOf(digit + " "));
                    stack_calculation.push(forhead);
                }
                return;
            } else if (lookahead.symbol == Symbol.ID1) {
                DigitOrLetter();
                return;
            } else if (lookahead.symbol == Symbol.ID2) {
                DigitOrLetter();
                return;
            } else if (lookahead.symbol == '(') {
                DigitOrLetter();
                return;
            } else {
                negetives_before = "Negative operator";
                return;
            }

        }
    }

    private void DigitOrLetter() throws Exception {
        if (lookahead.symbol == Symbol.NUM) {
            negetives_before = "CNL";
            forhead = lookahead;
            if (!Negative) {
                digit = ((Numbers) lookahead).value;
            } else {
                digit = -1 * ((Numbers) lookahead).value;
            }
            Negative = false;
            match(lookahead);
        } else if (lookahead.symbol == Symbol.ID1) {
            negetives_before = "CNL";
            stack_calculation.push(new Numbers(first_var));
            list_of_postfix.add(String.valueOf(first_var + " "));
            match(lookahead);
        } else if (lookahead.symbol == Symbol.ID2) {
            negetives_before = "adadletter";

            stack_calculation.push(new Numbers(second_var));
            list_of_postfix.add(String.valueOf(second_var + " "));
            match(lookahead);
        } else if (lookahead.symbol == '(') {
            match(new Token('('));
            Addition_Subtraction();
            match(new Token(')'));
        } else {
            throw new Error("Syntax Error on line: " + lexer.line);
        }
    }

    void match(Token t) throws Exception {
        if (lookahead.symbol == t.symbol) {
            lookahead = lexer.Checker();
        } else {
            throw new Exception(" Syntax(Match) Error on line: " + lexer.line);
        }
    }

    boolean var(String s) {
        for (int i = 0; i < s.length(); i++) {
            StringBuilder sb = new StringBuilder();
            if (Character.isLetter(s.charAt(i))) {
                sb.delete(0, sb.length());
                for (int j = 0; j < s.length(); j++) {
                    if (s.charAt(j) == 'x') {
                        connector.add(s.charAt(j));
                    }
                    if (s.charAt(j) == 'y') {
                        connector.add(s.charAt(j));
                    }
                }
                for (int j = i; j < s.length(); j++) {
                    if (s.charAt(j) == '(' || s.charAt(j) == ')' || s.charAt(j) == '+' || s.charAt(j) == '-'
                            || s.charAt(j) == '*' || s.charAt(j) == '/' || s.charAt(j) == '^'
                            || Character.isDigit(s.charAt(j))) {
                        break;
                    } else {

                        sb.append(s.charAt(j));
                    }
                    i = j;
                }

                String ss = sb.toString();
                Word w = (Word) Lexical_Analyzer.symboltable.get(ss);
                if (w == null) {
                    valuable = true;
                }
            }
        }
        identifier = connector.size();
        return valuable;
    }

    public static void main(String[] args) throws Exception {
        Scanner scn = new Scanner(System.in);
        System.out.println("please enter the equation");
        String str = scn.nextLine();
        Syntax_Analyzer parse = new Syntax_Analyzer(str);
        parse.Addition_Subtraction();
        for (int i = 0; i < list_of_postfix.size(); i++) {
            System.out.print(list_of_postfix.get(i) + " ");
        }
        System.out.println();
        System.out.println(((Numbers) parse.stack_calculation.pop()).value);
    }
}
